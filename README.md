# Hello express with Typescript

Using Expressjs with typescript

## Notes

Generate tsconfig.json with `tsc --init`.

## How to run

### Requirement
- Nodejs

### Installation

```shell
yarn
```

### Production

```shell
yarn build
```

```shell
yarn start
```

### Development 

```shell
yarn dev
```