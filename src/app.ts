import express, { Application, Request, Response, NextFunction } from 'express'

const app: Application = express()

app.get('/', (req: Request, res: Response, next: NextFunction) => {
  res.send('<h1 style="font-family:Helvetica;">Hello, World!</h1>')
})

app.listen(5000, () => console.log('Server running'))
